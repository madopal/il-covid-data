#!/usr/bin/env python
""" NOTES:
- The data is processed as a JSON from the Cook County API. Originally, I was loading it
as a CSV from their site, but there were some issues. The code in the load_data routine can
process a TSV/CSV, but it assumes the cases are flat. The JSON code sniffs the actual
case data out of the payload. There may be more data in the metadata/other sections, but
I haven't played with it yet
- When the data is processed, it is indexed by the field Case Number. The dictionary by_case is
created such that with any Case Number, you can go and look at the original, unprocessed
record data from the API
- The processed data pulls a subset of fields right now, in order to better figure out
what the state of each case is, whether or not it's flagged COVID (which right now is a
string check in all cause-related fields for "covid" or "coronavirus"), and whether or not
it's a pneumonia that's *not* classified as COVID.
- The location data is also massaged a bit such that there's now a city, zip, and lat_long.
Those are primarily taken from the incident fields. In the source data, they are tracking both
incident and residence location, but the incident fields appear to be more resolute. In the one
case I inspected, the incident appeared to be a residence on the corner of Drexel & 47th, so it
wasn't a hospital. We might be able to pull these locations and figure out if they're hospitals
and confirm how many fatalities happened in a residence vs. a hospital.
"""

import os
import sys
import json
import time
from datetime import datetime
from argparse import ArgumentParser
import pprint
from collections import defaultdict
import operator
import requests
import folium
from folium.plugins import TimestampedGeoJson
from branca.element import Template, MacroElement

LOCATION_MAPPINGS = {
    'Incident City': 'incident_city',
    'Incident Zip Code': 'incident_zip',
    'Incident Address': 'incident_address',
    'Residence City': 'residence_city',
    'Residence_Zip': 'residence_zip',
    'Residence Address': 'residence_address',
    'latitude': None,
    'longitude': None,
    'location': None
}

GOOGLE_API_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json'
GOOGLE_PLACE_API = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
COUNTY_URL = 'https://datacatalog.cookcountyil.gov/api/views/cjeq-bs86/rows.json'
DEFAULT_START_DATE = '2019-11-31'
ZIP_CODE_FILE = 'il_zip_codes.csv'
API_KEY_FILE = 'api_key.txt'
HOSPITAL_FILE = 'hospital_coords.json'
DIVINED_ADDRESS_FILE = 'divined_addresses.json'
CAUSE_HEADERS = [
    'Primary Cause', 'Primary Cause Line A',
    'Primary Cause Line B', 'Primary Cause Line C',
    'Secondary Cause'
]

def parse_cmd_args():
    """ Routine to setup argparse """
    parser = ArgumentParser()
    start_date = str(datetime.now()).split()[0]
    parser.add_argument('-f', '--data-file',
                        help='file with data')
    parser.add_argument('-z', '--zip-code-file',
                        help='file with zip codes',
                        default=ZIP_CODE_FILE)
    parser.add_argument('-s', '--start-date',
                        help='start date to use to check for possible COVID cases (default: {})'\
                            .format(DEFAULT_START_DATE),
                        default=DEFAULT_START_DATE)
    parser.add_argument('-e', '--end-date',
                        help='end date to use to check for possible COVID cases (default: {})'\
                            .format(start_date),
                        default=start_date)
    parser.add_argument('-g', '--get-data',
                        help='get data from county',
                        action='store_true')
    parser.add_argument('-p', '--possible-cases',
                        help='show possible cases',
                        action='store_true')
    parser.add_argument('-l', '--show-locations',
                        help='show histogram of locations',
                        action='store_true')
    parser.add_argument('-d', '--show-dates',
                        help='show histogram of dates',
                        action='store_true')
    parser.add_argument('-o', '--get-hospitals',
                        help='using Google API, get hospital locations and save to a file',
                        action='store_true')
    parser.add_argument('-u', '--show-unknown',
                        help='show unknown cases',
                        action='store_true')
    parser.add_argument('-k', '--api-key',
                        help='filename with Google API key for address lookup',
                        default=API_KEY_FILE)
    parser.add_argument('-m', '--map-style',
                        default='all',
                        choices=['Stamen Toner', 'CartoDB dark_matter', 'CartoDB Positron', 'OpenStreetMap', 'all'],
                        help='The style to use for the map')
    parser.add_argument('-v', '--verbose',
                        help='verbose output (mostly for debugging, not implemented yet)',
                        action='store_true')
    args = parser.parse_args()
    if args.map_style == 'all':
        args.map_style = ['Stamen Toner', 'CartoDB dark_matter', 'CartoDB Positron', 'OpenStreetMap']
    else:
        args.map_style = [args.map_style]
    return args

def process_county_json(json_data=None):
    """ Processes the JSON and returns a subsection """
    header_data = [x.get('name') for x in json_data['meta']['view']['columns']]
    file_data = []
    for entry in json_data.get('data'):
        file_data.append(
            dict(zip(header_data, entry)))
    return file_data

def load_data(file_name=None, primary_key=None, process_county=True):
    """ Given a file name, loads a data file """
    file_data = None
    if file_name.endswith('json'):
        with open(file_name, 'r') as data_file:
            temp_data = json.load(data_file)
            if process_county:
                file_data = process_county_json(json_data=temp_data)
            else:
                file_data = temp_data
    elif file_name.endswith('csv'):
        header_data = []
        with open(file_name, 'r') as data_file:
            if not primary_key:
                file_data = []
            else:
                file_data = {}
            for line in data_file:
                if not header_data:
                    header_data = line.strip().split(',')
                else:
                    data_line = dict(zip(header_data, line.strip().split(',')))
                    if primary_key:
                        if primary_key in header_data:
                            key = data_line[primary_key]
                            del data_line[primary_key]
                            file_data[key] = data_line
                        else:
                            print('Unable to find key: {}'.format(primary_key))
                            raise
                    else:
                        file_data.append(data_line)
    elif file_name.endswith('tsv'):
        header_data = None
        with open(file_name, 'r') as data_file:
            file_data = []
            for line in data_file:
                if not header_data:
                    header_data = line.strip().split('\t')
                else:
                    file_data.append(
                        dict(zip(header_data, line.strip().split('\t'))))
    else:
        print('File of unknown type: {}'.format(file_name))

    return file_data

def get_value(entry=None, key=None, default=None):
    """ Gets a value from a dict, checking for None in both
        presence of key and value
    """
    value = entry.get(key, default)
    if not value:
        value = default

    return value

def get_city_and_zip(entry=None, divined_info=None, zip_code_data=None, api_key=None):
    """ Given our normalized location data, try and figure
        out a single city, state, zip
    """
    unknown_items = [None, 'none', 'unknown', 'not on file']
    result = {'city': 'unknown', 'address': None, 'zip': None, 'from': None}

    # get the city
    if entry['incident_city'] not in unknown_items:
        result['city'] = entry['incident_city']
        result['from'] = 'incident'
    elif entry['residence_city'] not in unknown_items:
        result['city'] = entry['residence_city']
        result['from'] = 'residence'

    if entry['incident_zip'] not in unknown_items:
        if result['from']:
            if result['from'] is 'incident':
                result['zip'] = entry['incident_zip']
                result['from'] = 'incident'
        else:
            result['zip'] = entry['incident_zip']
            result['from'] = 'incident'

    elif entry['residence_zip'] not in unknown_items:
        if result['from']:
            if result['from'] is 'residence':
                result['zip'] = entry['residence_zip']
                result['from'] = 'residence'
        else:
            result['zip'] = entry['residence_zip']
            result['from'] = 'residence'

    if entry['incident_address'] not in unknown_items:
        if result['from']:
            if result['from'] is 'incident':
                result['address'] = entry['incident_address']
                result['from'] = 'incident'
        else:
            result['address'] = entry['incident_address']
            result['from'] = 'incident'

    elif entry['residence_address'] not in unknown_items:
        if result['from']:
            if result['from'] is 'residence':
                result['address'] = entry['residence_address']
                result['from'] = 'residence'
        else:
            result['address'] = entry['residence_address']
            result['from'] = 'residence'

    # if we don't have a city, see if we can figure one out
    if result['city'] in unknown_items:
        # try using the zip code
        if result['zip'] not in unknown_items:
            result['city'] = zip_code_data.get(result['zip'], {}).get('town', 'none').lower()
        if result['city'] in unknown_items:
            # figure out which fields we have we could look up
            divined_key = {}
            if entry['lat_long'] not in unknown_items:
                divined_key['latlng'] = entry['lat_long']
            elif result['address'] not in unknown_items:
                divined_key['address'] = result['address']

            # if we have one, see what we can figure out
            if divined_key:
                for _, div_key_val in divined_key.items():
                    # if we've already figured this out, use it
                    if div_key_val in divined_info:
                        #print('Previous result found: {}'.format(div_key_val))
                        result.update(divined_info[div_key_val])
                    # otherwise, go look it up
                    else:
                        print('Looking up on API')
                        if api_key:
                            payload = None
                            if entry['lat_long'] not in unknown_items:
                                payload = {
                                    'latlng': entry['lat_long'],
                                    'key': api_key
                                }
                            elif result['address'] not in unknown_items:
                                payload = {
                                    'address': result['address'],
                                    'key': api_key
                                }
                            if payload:
                                error = None
                                req = requests.get(GOOGLE_API_BASE_URL, params=payload)
                                if req.status_code == 200:
                                    loc_data = req.json()
                                    if loc_data.get('status') == 'OK':
                                        for loc_entry in loc_data.get('results', []):
                                            for component in loc_entry.get('address_components'):
                                                if 'locality' in component.get('types', []):
                                                    result['city'] = component.get(
                                                        'short_name', '').lower()
                                                if 'postal_code' in component.get('types', []):
                                                    if result['zip'] in [None, 'none']:
                                                        result['zip'] = component.get(
                                                            'short_name', '')
                                        for _, div_val in divined_key.items():
                                            divined_info[div_val] = {
                                                'city': result['city'],
                                                'address': result['address'],
                                                'zip': result['zip']
                                            }
                                    else:
                                        error = loc_data.get('status')
                                else:
                                    error = '{}: {}'.format(req.status_code, req.reason)

                                if error:
                                    print('Unable to make API call:')
                                    print(error)

    return result


def normalize_result(entry=None,
                     divined_data=None,
                     zip_code_data=None,
                     start_date=None,
                     end_date=None,
                     api_key=None):
    """ Given a record, normalize the results """

    possible_string_checks = [
        #'covid',
        'pneumonia',
        #'ards'
    ]
    corona_other_checks = [
        'oc43', 'hku1', '229e', 'nl63'
    ]
    covid_checks = [
        'coronavirus', 'covid'
    ]
    result = {
        'covid': False,
        'possible_covid': False,
        'lat_long': None
    }

    death_date = entry.get('Date of Death').split('T')[0]
    if (death_date >= start_date) and (death_date <= end_date):
        if entry.get('Manner of Death') == 'NATURAL':
            # normalize all location data
            for key1, key2 in LOCATION_MAPPINGS.items():
                if key2:
                    result[key2] = get_value(entry=entry, key=key1, default='unknown').lower()
            # try and find the specific location data
            if entry.get('latitude') and entry.get('longitude'):
                result['lat_long'] = (
                    entry.get('latitude'),
                    entry.get('longitude'))
            if not result['lat_long']:
                if entry.get('location'):
                    result['lat_long'] = (
                        entry.get('location')[1],
                        entry.get('location')[2])
            result.update(get_city_and_zip(entry=result,
                                           zip_code_data=zip_code_data,
                                           divined_info=divined_data,
                                           api_key=api_key))

            # normalize clinical data
            result['age'] = get_value(entry=entry, key='Age', default='none')
            result['gender'] = get_value(entry=entry, key='Gender', default='none')
            result['race'] = get_value(entry=entry, key='Race', default='none')
            result['latino'] = get_value(entry=entry, key='Latino', default=False)
            # normalize date data
            created_datetime = datetime.fromtimestamp(entry.get('created_at'))
            updated_datetime = datetime.fromtimestamp(entry.get('updated_at'))
            result['created'] = created_datetime.strftime('%Y-%m-%dT%H:%M:%S')
            result['updated'] = updated_datetime.strftime('%Y-%m-%dT%H:%M:%S')
            result['fatality_date'] = entry.get('Date of Death')
            result['incident_date'] = entry.get('Date of Incident')
            result['fatality_day'] = death_date
            # normalize cause data
            result['causes'] = {}
            for header in CAUSE_HEADERS:
                # get the causes to save them
                if entry.get(header):
                    result['causes'][header] = entry.get(header)

                # check if covid
                cur_cause = entry.get(header, 'none')
                if not cur_cause:
                    cur_cause = 'none'
                other_corona = False
                for covid_check in covid_checks:
                    if covid_check in cur_cause.lower():
                        for corona_other in corona_other_checks:
                            if corona_other in cur_cause.lower():
                                other_corona = True
                                break
                        if not other_corona:
                            result['covid'] = True
                        else:
                            result['possible_covid'] = True
                            result['other_corona'] = True
                # check if possible covid
                for cause in possible_string_checks:
                    if cause in cur_cause.lower():
                        result['possible_covid'] = True

    return result

def get_hospitals(api_key=None):
    """ Get the hospitals from the Google API """
    hospitals = []
    if api_key:
        payload = {
            'location': '41.882057, -87.627816',
            'radius': '15000',
            'type': 'hospital',
            'key': api_key
        }
        req = requests.get(GOOGLE_PLACE_API, params=payload)
        checking = True
        while checking:
            print('{} results'.format(len(req.json().get('results', []))))
            for hospital in req.json().get('results', []):
                hosp_loc = hospital.get('geometry', {}).get('location')
                hospital_data = {
                    'lat_long': '{}, {}'.format(hosp_loc.get('lat'), hosp_loc.get('lng')),
                    'name': hospital.get('name'),
                    'address': hospital.get('vicinity')}
                hospitals.append(hospital_data)
            if req.json().get('next_page_token'):
                print('Checking with next token')
                time.sleep(5)
                payload['pagetoken'] = req.json().get('next_page_token')
                req = requests.get(GOOGLE_PLACE_API, params=payload)
            else:
                checking = False
    return hospitals


def get_hospitals():
    with open(HOSPITAL_LIST_FILE, 'r') as f:
        hospitals = json.load(f)
    return hospitals


def generate_map(fatalities, style):
    m = get_map(style)

    for case_id, case in fatalities.items():
        if case['lat_long']:
            popup = '<br/>'.join(['{}: {}'.format(k, case[k]) for k in ('gender', 'age', 'race', 'fatality_date')])
            folium.Circle([float(case['lat_long'][0]), float(case['lat_long'][1])], radius=200, color='crimson', fill=False, popup=popup).add_to(m)

    return m


def get_map(style):
    m = folium.Map(location=[41.837649, -87.767817],
               tiles=style,
               zoom_start=11)


    template = """
    {% macro html(this, kwargs) %}

    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Sympatic - Covid-19 Fatalities</title>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <script>
      $( function() {
        $( "#maplegend" ).draggable({
                        start: function (event, ui) {
                            $(this).css({
                                right: "auto",
                                top: "auto",
                                bottom: "auto"
                            });
                        }
                    });
    });

      </script>
    </head>
    <body>

    <div class='maplegend' style='position: absolute; z-index:9999; padding: 10px; right: 5px; top: 20px;'>
         <img src='new_logo_horiz_blue_to_green.png' width=200 />
    </div>

    <div id='maplegend' class='maplegend'
        style='position: absolute; z-index:9999; border:2px solid grey; background-color:rgba(255, 255, 255, 0.8);
         border-radius:6px; padding: 10px; font-size:14px; right: 20px; top: 75px;'>

    <div class='legend-title'>Legend</div>
    <div class='legend-scale'>
      <ul class='legend-labels'>
        <li><span style='background:crimson;'></span>Fatalities</li>
        <li><span style='background:#2173C3;'></span>Hospitals</li>
      </ul>
    </div>
    </div>

    </body>
    </html>

    <style type='text/css'>
      .maplegend .legend-title {
        text-align: left;
        margin-bottom: 5px;
        font-weight: bold;
        font-size: 90%;
        }
      .maplegend .legend-scale ul {
        margin: 0;
        margin-bottom: 5px;
        padding: 0;
        float: left;
        list-style: none;
        }
      .maplegend .legend-scale ul li {
        font-size: 80%;
        list-style: none;
        margin-left: 0;
        line-height: 18px;
        margin-bottom: 2px;
        }
      .maplegend ul.legend-labels li span {
        display: block;
        float: left;
        height: 16px;
        width: 30px;
        margin-right: 5px;
        margin-left: 0;
        border: 1px solid #999;
        }
      .maplegend .legend-source {
        font-size: 80%;
        color: #777;
        clear: both;
        }
      .maplegend a {
        color: #777;
        }
    </style>
    {% endmacro %}"""

    macro = MacroElement()
    macro._template = Template(template)

    m.get_root().add_child(macro)


    hospitals = get_hospitals()

    for hospital in hospitals:
        #popup = "<br/>".join([hospital['name'], hospital['address']])
        popup = hospital['name']
        coord = hospital['lat_long'].split(',')
        folium.Circle([float(coord[0]), float(coord[1])], radius=300, color='#2173C3', fill=False, popup=popup).add_to(m)

    return m


def generate_animated_map(fatalities, style):
    m = get_map(style)
    features = []

    for case_id, case in fatalities.items():
        if case['lat_long']:
            #popup = '<br/>'.join(['{}: {}'.format(k, case[k]) for k in ('gender', 'age', 'race', 'fatality_date')])
            feature = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [float(case['lat_long'][1]), float(case['lat_long'][0])]
                },
                'properties': {
                    'time': case['fatality_date'].split('T')[0]+' 00:00:00',
                    'style': {'color' : 'crimson'},
                    'icon': 'circle',
                    'iconstyle':{
                        'fillColor': 'crimson',
                        'fillOpacity': 0.8,
                        'stroke': 'true',
                        'radius': 3
                    }
                }
            }
            features.append(feature)

    TimestampedGeoJson(features,
                  period='P1D',
                  duration=None,
                  transition_time=200,
                  loop=False,
                  loop_button=True,
                  auto_play=True).add_to(m)

    return m


def main():
    """ Main routine """
    args = parse_cmd_args()
    data = None
    api_key = None
    pretty_printer = pprint.PrettyPrinter(indent=2)
    if os.path.isfile(DIVINED_ADDRESS_FILE):
        divined_info = load_data(file_name=DIVINED_ADDRESS_FILE,
                                 process_county=False)
    else:
        divined_info = {}
    if os.path.isfile(args.api_key):
        with open(args.api_key, 'r') as in_file:
            for line in in_file:
                api_key = line.strip()
                break
        if api_key:
            if args.get_hospitals:
                hospitals = get_hospitals(api_key=api_key)
                print('{} hospitals found'.format(len(hospitals)))

                with open(HOSPITAL_FILE, 'w') as out_file:
                    json.dump(hospitals, out_file)
                print('Saved to {}'.format(HOSPITAL_FILE))
                sys.exit()

    if args.data_file:
        data = load_data(file_name=args.data_file)
    elif args.get_data:
        try:
            base_data = requests.get(COUNTY_URL)
        except Exception as exception:
            print('Unable to get data from {}'.format(COUNTY_URL))
            print(exception)
            sys.exit()

        data = process_county_json(json_data=base_data.json())

    if data:
        zip_code_data = load_data(
            file_name=args.zip_code_file,
            primary_key='zip')
        by_case = {}
        covid_fatalities = {}
        possible_covid_fatalities = {}
        covid_fatality_locations = defaultdict(list)
        possible_covid_fatality_locations = defaultdict(list)
        chicago = []
        non_chicago = []
        unknown = []
        chicago_fatality_histogram = defaultdict(int)
        non_chicago_fatality_histogram = defaultdict(int)
        last_update = '2000-01-01T00:00:00'
        for entry in data:
            case_num = entry.get('Case Number')
            by_case[case_num] = entry
            summary_data = normalize_result(
                entry=entry,
                start_date=args.start_date,
                end_date=args.end_date,
                divined_data=divined_info,
                zip_code_data=zip_code_data,
                api_key=api_key)

            if summary_data.get('updated', '2000-01-01T00:00:00') > last_update:
                last_update = summary_data['updated']
            if summary_data['covid']:
                if summary_data['fatality_date'].startswith('2020-02'):
                    print('Fatality: {} ({}), created: {}, updated: {}'.format(
                        summary_data['fatality_date'],
                        summary_data['fatality_day'],
                        summary_data['created'],
                        summary_data['updated']))
                    print(summary_data)
                covid_fatalities[case_num] = summary_data
                covid_fatality_locations[summary_data['city']].append(case_num)
                if summary_data['city'] == 'chicago':
                    chicago_fatality_histogram[summary_data['fatality_day']] += 1
                    chicago.append(case_num)
                elif summary_data['city'] == 'unknown':
                    unknown.append(case_num)
                    if args.show_unknown:
                        print('Case Number: {}'.format(case_num))
                        #for key in LOCATION_MAPPINGS.keys():
                        #    print(key, entry.get(key))
                        pretty_printer.pprint(entry)
                        print()
                else:
                    non_chicago_fatality_histogram[summary_data['fatality_day']] += 1
                    non_chicago.append(case_num)
            elif summary_data['possible_covid']:
                possible_covid_fatalities[case_num] = summary_data
                possible_covid_fatality_locations[summary_data['city']].append(case_num)

        if args.show_locations:
            sorted_locations = sorted(
                covid_fatality_locations.items(),
                key=lambda key_value_pair: (len(key_value_pair[1]), key_value_pair[0]),
                reverse=True)
            for entry in sorted_locations:
                print(entry[0], len(entry[1]))

        with open(DIVINED_ADDRESS_FILE, 'w') as divined_file:
            json.dump(divined_info, divined_file)
        if args.show_dates:
            sorted_dates = sorted(
                chicago_fatality_histogram.items(),
                key=operator.itemgetter(0),
                reverse=True)
            print('{:10s} {:>3s} {:>3s}'.format('date', 'chi', 'non'))
            print('{:10s} {:>3s} {:>3s}'.format('----', '---', '---'))
            for entry in sorted_dates:
                print('{} {:3d} {:3d}'.format(entry[0],
                                              chicago_fatality_histogram[entry[0]],
                                              non_chicago_fatality_histogram[entry[0]]))
        print('Last updated: {}'.format(last_update))
        print('{} covid fatalities found'.format(len(covid_fatalities)))
        print('Chicago: {}, Non-Chicago: {}, unknown: {}'.format(
            len(chicago), len(non_chicago), len(unknown)))
        if args.possible_cases:
            print('{} possible covid fatalities found between {} and {}'.format(
                len(possible_covid_fatalities), args.start_date, args.end_date))
            for entry in possible_covid_fatalities:
                causes = []
                for cause in CAUSE_HEADERS:
                    if by_case[entry].get(cause):
                        causes.append(by_case[entry].get(cause))
                print('{}: {} - {}'.format(
                    by_case[entry].get('Case Number'),
                    by_case[entry].get('Date of Death'),
                    causes))
        file_timestamp = time.time()

        copyfile(LOGO_FILENAME, 'output/{}'.format(LOGO_FILENAME))
        for map_style in args.map_style:
            style_name = map_style.replace(' ', '_').lower()
            m = generate_map(covid_fatalities, style=map_style)
            m.save('output/fatalities_{}.html'.format(style_name))
            m = generate_animated_map(covid_fatalities, style=map_style)
            m.save('output/animated_fatalities_{}.html'.format(style_name))

    else:
        print("You must provide either a file '-f' or get the data '-g'")


if __name__ == '__main__':
    main()
