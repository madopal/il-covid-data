#!/usr/bin/env python

import os
import sys
import json
import time
from datetime import datetime
from argparse import ArgumentParser
import pprint
from collections import defaultdict
import operator
import requests

GOOGLE_API_BASE_URL='https://maps.googleapis.com/maps/api/geocode/json'
API_KEY_FILE='api_key.txt'

def parse_cmd_args():
    """ Routine to setup argparse """
    parser = ArgumentParser()
    start_date = str(datetime.now()).split()[0]
    parser.add_argument('-d', '--data-dir',
                        help='directory with data files')
    parser.add_argument('-k', '--api-key',
                        help='filename with Google API key for address lookup',
                        default=API_KEY_FILE)

    args = parser.parse_args()
    return args

def load_data(file_name=None, primary_key=None, count=None):
    file_data = None
    delimeter = None
    if file_name.endswith('json'):
        with open(file_name, 'r') as data_file:
            temp_data = json.load(data_file)
            file_data = temp_data
    elif file_name.endswith('csv'):
        delimeter = ','

    elif file_name.endswith('tsv'):
        delimeter = '\t'
    else:
        print('File of unknown type: {}'.format(file_name))
        
    if delimeter:
        header_data = None
        with open(file_name, 'r') as data_file:
            if not primary_key:
                file_data = []
            else:
                file_data = {}
            for line in data_file:
                if not header_data:
                    header_data = line.strip().split(delimeter)
                else:
                    data_line = dict(zip(header_data, line.strip().split(delimeter)))
                    if primary_key:
                        if primary_key in header_data:
                            key = data_line[primary_key]
                            del data_line[primary_key]
                            file_data[key] = data_line
                        else:
                            print('Unable to find key: {}'.format(primary_key))
                            raise
                    else:
                        file_data.append(data_line)
                    if count:
                        count -= 1
                        if count <= 0:
                            break

    return file_data

def lookup_hospital(hosp_data=None, api_key=None):
    lat_long = None
    if api_key:
        payload = {
            'address':'{}, {}, IL'.format(
                hosp_data.get('Street Address'),
                hosp_data.get('City')),
            'key': api_key
        }
        print('Looking for {}'.format(payload['address']))
        req = requests.get(GOOGLE_API_BASE_URL, params=payload)
        if req.status_code == 200:
            loc_data = req.json()
            if loc_data.get('status') == 'OK':
                for entry in loc_data.get('results', []):
                    lat_long = '{},{}'.format(
                        entry.get('geometry', {}).get('location', {}).get('lat'),
                        entry.get('geometry', {}).get('location', {}).get('lng'))
            else:
                error = loc_data.get('status')
        else:
            error = '{}: {}'.format(req.status_code, req.reason)
    return lat_long


def main():
    args = parse_cmd_args()
    api_key=None
    if os.path.isfile(args.api_key):
        with open(args.api_key, 'r') as in_file:
            for line in in_file:
                api_key = line.strip()
                break
    hospital_data = None
    for (path, names, filenames) in os.walk(args.data_dir):
        for filename in filenames:
            path_filename = '{}/{}'.format(path, filename)
            tsv_data = load_data(file_name=path_filename,
                primary_key='Hospital ID')
            if not hospital_data:
                hospital_data = tsv_data
            else:
                for key, entry in tsv_data.items():
                    if key:
                        hospital_data[key].update(entry)
    print('{} hospitals found'.format(len(hospital_data.keys())))
    cook_county_hospitals = []
    for key, value in hospital_data.items():
        if value.get('County').lower() == 'cook':
            lat_long = lookup_hospital(hosp_data=value, api_key=api_key)
            value['lat_long'] = lat_long
            cook_county_hospitals.append({
                'name': value.get('Hospital'),
                'city': value.get('City'),
                'address': value.get('Street Address'),
                'lat_long': value.get('lat_long')
            })
    print('Writing {} Cook County hospitals'.format(len(cook_county_hospitals)))
    with open('cook_county_hospitals.json', 'w') as hosp_file:
        json.dump(cook_county_hospitals, hosp_file)
if __name__ == '__main__':
    main()

