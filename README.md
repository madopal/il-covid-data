# il-covid-data

Repository gathering Illinois COVID-19 data gathering/analysis tools

usage: main.py [-h] [-f DATA_FILE] [-z ZIP_CODE_FILE] [-d CUTOFF_DATE] [-g]
               [-p] [-l] [-u] [-k API_KEY]

optional arguments:
  -h, --help            show this help message and exit
  -f DATA_FILE, --data-file DATA_FILE
                        file with data
  -z ZIP_CODE_FILE, --zip-code-file ZIP_CODE_FILE
                        file with zip codes
  -d CUTOFF_DATE, --cutoff-date CUTOFF_DATE
                        date to use for checking for possible COVID cases
                        (default: 2019-11-31)
  -g, --get-data        get data from county
  -p, --possible-cases  show possible cases
  -l, --show-locations  show breakdown of locations
  -u, --show-unknown    show unknown cases
  -k API_KEY, --api-key API_KEY
                        filename with Google API key for address lookup

The tool can pull from the County API or load a file, run with -h for help
The il_zip_codes.csv file was pulled from online and modified to add Chicago
neighborhood data

The code can now resolve addresses/lat-long to a full location using the 
Google geosjon API. The code looks for api_key.txt by default, with just
a key expected in a single line in the file. It will skip this capability
if the API isn't found. 

In addition, once locations are resolved using the API, those results are
saved in a json called divined_addresses.json, which is now included. Any
new addresses that are resolved will be updated in there, so only one call
is necessary to update. It will create this file if it is missing.
