FROM python:3.7-alpine3.11
MAINTAINER Sympatic, Inc.  <christian@sympatic.com>

RUN apk update
RUN apk upgrade
RUN apk add --update python python-dev gfortran py-pip build-base

RUN pip install --upgrade pip

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir ./input
RUN mkdir ./output
COPY main.py ./
COPY new_logo_horiz_blue_to_green.png ./


ENTRYPOINT ["python"]

CMD ["./main.py", "-g"]
